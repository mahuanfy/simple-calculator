import React, {Component} from 'react';
import './calculator.less';
import InputBox from "./operation/inputBox/InputBox";
import ResultBox from "./resultBox/ResultBox";
import Operation from "./operation/Operation";

class Calculator extends Component {
  constructor(props){
    super(props);
    this.state = {
      result: ''
    }
  }
  getResult(result){
    this.setState({result})
  }
  clearValueAndResult(){
    this.setState({
      result: ''
    })
  }
  render() {
    const {result} = this.state;
    return (
      <div className='main'>
        <ResultBox value={result}/>
        <Operation
          getResult={this.getResult.bind(this)}
          clearValueAndResult={this.clearValueAndResult.bind(this)}
        />
      </div>
    );
  }
}

export default Calculator;


