import React from 'react'
import OperationBox from "./operation-box/OperationBox";
import InputBox from "./inputBox/InputBox";
import './operation.less'

class Operation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstNumber: 0,
      secondNumber: 0,
      operation: ''
    }
  }

  additiveOperation() {
    this.setState({operation: '+'});
  }

  subtraction() {
    this.setState({operation: '-'});
  }

  multiplication() {
    this.setState({operation: '*'});
  }

  divisionOperation() {
    this.setState({operation: '/'});
  }

  displayResult() {
    const result = this.calculate();
    this.props.getResult(result);
  }

  handleChangeFirstValue(value) {
    this.setState({firstNumber: value})
  }

  handleChangeSecondValue(value) {
    this.setState({secondNumber: value})
  }

  calculate() {
    const {firstNumber, secondNumber, operation} = this.state;
    switch (operation) {
      case '+':
        return parseInt(firstNumber) + parseInt(secondNumber);
      case '-':
        return firstNumber - secondNumber;
      case '*':
        return firstNumber * secondNumber;
      case '/':
        return firstNumber / secondNumber;
    }
  }

  render() {
    const {firstValue, secondValue, handleChangeFirstValue, handleChangeSecondValue, clearValueAndResult} = this.props
    return (
      <>
        <div className="left">
          <OperationBox clazzName="operation-button" type="+" handleClick={this.additiveOperation.bind(this)}/>
          <OperationBox clazzName="operation-button" type="x" handleClick={this.multiplication.bind(this)}/>
          <OperationBox clazzName="operation-button" type="-" handleClick={this.subtraction.bind(this)}/>
          <OperationBox clazzName="operation-button" type="/" handleClick={this.divisionOperation.bind(this)}/>
          <OperationBox clazzName="settlement-button" type="AC" handleClick={clearValueAndResult}/>
        </div>
        <div className="right">
          <InputBox
            value={firstValue}
            handleChange={this.handleChangeFirstValue.bind(this)}/>
          <InputBox
            value={secondValue}
            handleChange={this.handleChangeSecondValue.bind(this)}/>
          <OperationBox clazzName="settlement-button" type="=" handleClick={this.displayResult.bind(this)}/>
        </div>
      </>
    )
  }
}

export default Operation