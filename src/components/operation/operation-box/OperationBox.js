import React from 'react'
import './operation-box.less'

const OperationBox = (props) => {
  const {type, handleClick, clazzName} = props;
  return (
    <button
      className={clazzName}
      onClick={handleClick}>
      {type}
    </button>
  )
}
export default OperationBox