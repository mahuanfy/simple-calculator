import React from 'react'
import './inputBox.less'
const InputBox = (props) => {
  const {value, handleChange} = props;
  return <label>
    <input
      value={value}
      onChange={event => handleChange(event.target.value)}
      className='input-box'
    />
  </label>
}
export default InputBox