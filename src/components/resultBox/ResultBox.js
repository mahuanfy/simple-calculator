import React from 'react'
import './resultBox.less'
const ResultBox = (props) =>{
  const {value} = props;
  return <label>
      <input
        readOnly
        value={value}
        className="result-input"
      />
  </label>
}
export default ResultBox